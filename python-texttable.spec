%global pkg     texttable

Name:           python-%{pkg}
Version:        0.8.1
Release:        1%{?dist}
Summary:        Python module for creating simple ASCII tables

Packager:       Eugene Zamriy <eugene@zamriy.info>

Group:          Development/Languages
License:        LGPL
URL:            http://foutaise.org/code/
Source0:        http://foutaise.org/code/%{pkg}/%{pkg}-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  python-devel
BuildRequires:  python-setuptools-devel


%description
%{pkg} is a Python module to generate a formatted text table, using ASCII characters.


%prep
%setup -q -n %{pkg}-%{version}

%build
%{__python} setup.py build

%install
%{__rm} -rf %{buildroot}
%{__python} setup.py install --skip-build --root %{buildroot}

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{python_sitelib}/*


%changelog
* Sun May 12 2013 Eugene G. Zamriy <eugene@zamriy.info> - 0.8.1-1
- initial release
